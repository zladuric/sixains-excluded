const { sep } = require('path');

const ROOTDIR_NAME_PLACEHOLDER = '==GALLERY==';

const galleryDefaults = {
  template: 'gallery.pug', // template that renders pages
  indexTemplate: 'galleries-index.pug', // template for categories home
  galleries: 'galleries', // directory containing contents to paginate
  validImageTypes: ['png', 'jpg', 'jpeg'], // which types of templates are valid for inclusion
  galleriesIndex: `galleries${sep}index.html`,
  galleryPageIndex: `${ROOTDIR_NAME_PLACEHOLDER}${sep}index.html`, // filename/url for gallery page page
  galleriesRootMandatory: true, // set false if you want to make gallery optional..
};

module.exports = {
  ROOTDIR_NAME_PLACEHOLDER,
  galleryDefaults,
};
